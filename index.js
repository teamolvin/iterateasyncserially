/**
 * Repeatedly calls `callback`, awaiting the results, iterating over each of
 * the items in `items`. The `callback` receives a single item as first and
 * only argument.
 *
 * @param {Array} items
 * @param {*} callback
 */

function iterateAsyncSerially(items, callback) {
  // serially iterate over tables
  return items.reduce(
    (promise, item, idx) => promise.then(() => callback(item, idx)),
    Promise.resolve()
  );
}

module.exports = iterateAsyncSerially;

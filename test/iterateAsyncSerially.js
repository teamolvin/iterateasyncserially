const { expect } = require('chai');

const iterateAsyncSerially = require('../index');

describe('iterateAsyncSerially', () => {
  it('iterates', async () => {
    const items = ['aap', 'noot', 'mies'];
    let res = '';
    await iterateAsyncSerially(items, item => {
      res += item;
    });
    expect(res).to.equal('aapnootmies');
  });

  it('receives idx', async () => {
    const items = ['aap', 'noot', 'mies'];
    const indexes = [];
    await iterateAsyncSerially(items, (item, idx) => {
      indexes.push(idx);
    });
    expect(indexes).to.deep.equal([0, 1, 2]);
  });
});
